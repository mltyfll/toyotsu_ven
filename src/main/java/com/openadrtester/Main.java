package main.java.com.openadrtester;

import java.io.IOException;
import java.util.Scanner;

import main.java.jp.nuvve.aggregator.openadr.ISOConnectionLauncherCert;

public class Main {
    public static void main(String[] args) throws IOException, RuntimeException {
        
        ISOConnectionLauncherCert launcher = new ISOConnectionLauncherCert();
        launcher.start();
        while(true){
            System.out.println("simple tests for OpenADR interface...");
            System.out.println("type a number of following test case:");
            
            Scanner scanner = new Scanner(System.in);
            int selected_menu = scanner.nextInt();
            if (selected_menu != 0 && selected_menu < 99999){
                System.out.println("selected menu: [" + selected_menu + "]");
    
                switch(selected_menu){
                case 5010: 
                	launcher.sendQueryRegistration();
                	break;
                default:
                	break; 
                }
                
            } else if(selected_menu == 0){
                launcher.stop();
                break;
            }
        }
    }
}