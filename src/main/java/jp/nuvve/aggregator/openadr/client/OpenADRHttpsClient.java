package main.java.jp.nuvve.aggregator.openadr.client;
   
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.KeyStore;
import java.io.InputStream;
import java.io.FileInputStream;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.io.FileNotFoundException;
import java.lang.RuntimeException;
import org.apache.http.ssl.SSLContexts;
import java.security.UnrecoverableKeyException;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class OpenADRHttpsClient{

private static String httpsURL = "https://vtn:8080/OpenADR2/Simple/2.0b/";
private static int timeout = 5000;
private HttpsURLConnection con;

// set keystore and truststore
private static String KEYSTORE_PATH = "/Users/hayashinana/cert/ven/VEN_keystore.jks";
private static String TRUSTSTORE_PATH = "/Users/hayashinana/cert/ven/VEN_truststore.jks";
private static char[] KeyStorePass = "password".toCharArray();
private static String TrustStorePass = "password";

public OpenADRHttpsClient(boolean tmcFlg, boolean wuFlg) {
}

public void setup(){
}

private URL getURLObject(String eventName) throws MalformedURLException{
    URL url = new URL(httpsURL + eventName);
    return url;
}

private KeyStore readStore() throws RuntimeException {
    try (InputStream keyStoreStream = new FileInputStream(KEYSTORE_PATH)) {
        KeyStore keyStore = KeyStore.getInstance("PKCS12"); // PKCS12 or "JKS"
        keyStore.load(keyStoreStream, KeyStorePass);
        return keyStore;
      } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
        throw new RuntimeException(e);
      }
  }

// HTTP POST request
public String sendPost(String payload, String eventName) throws IOException, RuntimeException{

    URL url = getURLObject(eventName); // will be replaced by getURLObject()
    con = (HttpsURLConnection)url.openConnection();
    final KeyStore keyStore = readStore();
    System.out.println(payload);
    // create the SSL connection
    SSLContext sslContext;
	try {
		sslContext = SSLContext.getInstance("TLSv1.2");
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(keyStore, KeyStorePass);
		sslContext.init(kmf.getKeyManagers(), null, null);
		System.setProperty("javax.net.ssl.trustStore", TRUSTSTORE_PATH);
		System.setProperty("javax.net.ssl.trustStorePassword", TrustStorePass);
		System.setProperty("https.cipherSuites", "TLS_RSA_WITH_AES_128_CBC_SHA256");
    } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException | UnrecoverableKeyException e) {
		throw new RuntimeException(e);
	}
    con.setSSLSocketFactory(sslContext.getSocketFactory());
	con.setHostnameVerifier(new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	});

    //add reuqest header
    con.setRequestMethod("POST");
    con.setConnectTimeout(timeout);
    con.setRequestProperty("Content-Type", "application/xml; charset=utf-8");
    con.addRequestProperty("Content-Length", String.valueOf(payload.length()));
    // con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

    // Send post request
    con.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
    System.out.println(payload);
    wr.writeBytes(payload);
    wr.flush();
    wr.close();

    int responseCode = con.getResponseCode();

    BufferedReader in = new BufferedReader(
            new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
    }
    in.close();
    
    String msg = response.toString();
    return msg;

}
}
