package main.java.jp.nuvve.aggregator.openadr;

import main.java.jp.nuvve.aggregator.openadr.connection.OpenADRConnectionCert;

/**
 * @author Nana Hayashi
 *
 */
public class ISOConnectionLauncherCert {
    private OpenADRConnectionCert isoConn;

    public ISOConnectionLauncherCert() {
        this.isoConn = new OpenADRConnectionCert();
    }

    /**
     * OpenADRConnectionCert-setup()
     */
    public void start(){
        this.isoConn.setup();
    }
    
    /**
     * OpenADRConnectionCert-setup()
     */
    public void stop(){
        this.isoConn.stop();
    }

    /**
     * OpenADRConnectionCert-sendQueryRegistration()
     */
    public void sendQueryRegistration() {
    	this.isoConn.sendQueryRegistration();
    }

}