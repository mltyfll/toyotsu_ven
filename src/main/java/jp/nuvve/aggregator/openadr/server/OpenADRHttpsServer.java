package main.java.jp.nuvve.aggregator.openadr.server;

import java.io.*;
import java.net.*;
import javax.net.ssl.*;
import com.sun.net.httpserver.*;
import main.java.jp.nuvve.aggregator.openadr.connection.OpenADRConnectionCert;

import java.security.*;
import java.security.cert.CertificateException;
 
public class OpenADRHttpsServer {
	// set keystore and truststore
    private static String KEYSTORE_PATH = "/Users/hayashinana/cert/ven/VEN_keystore.jks";
    private static String TRUSTSTORE_PATH = "/Users/hayashinana/cert/ven/VEN_truststore.jks";
    private static char[] KeyStorePass = "password".toCharArray();
    private static char[] TrustStorePass = "password".toCharArray();
    
    private static String oadrPrefix = "/OpenADR2/Simple/2.0b";
    private KeyManagerFactory kmf;
    private TrustManagerFactory tmf;
    private KeyStore keystore;
    private KeyStore truststore;
    private HttpsServer server;
    private OpenADRConnectionCert adrConn;
    private static int port;


    public OpenADRHttpsServer(OpenADRConnectionCert con){
        this.adrConn = con;
        port = 8082;
    }

    public void run() throws IOException, NoSuchAlgorithmException, KeyManagementException{
            server = HttpsServer.create(new InetSocketAddress(port), 0);
            //server = HttpServer.create(new InetSocketAddress(port), 0);
            this.loadCertificate();
    
            // create sslContext
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            server.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
                public void configure(HttpsParameters params) {
                    try {
                                // initialise the SSL context
                                SSLContext c = SSLContext.getInstance("TLSv1.2");
                                c.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
                                SSLContext.setDefault(c);
                                SSLParameters sslParams = c.getDefaultSSLParameters();
                                SSLEngine engine = c.createSSLEngine();
                                String[] cipherSuites = new String[1];
                                cipherSuites[0] = "TLS_RSA_WITH_AES_128_CBC_SHA256";
                                engine.setWantClientAuth(true);
                                params.setNeedClientAuth(true);
                                sslParams.setNeedClientAuth(true);
                                //params.setCipherSuites(engine.getEnabledCipherSuites());
                                params.setCipherSuites(cipherSuites);
                                params.setProtocols(engine.getEnabledProtocols());
                                params.setSSLParameters(sslParams);
                    } catch (Exception ex) {
                                ex.printStackTrace();
                                System.out.println("Failed to create HTTPS server");
                    }
                }
            });
            
            server.createContext(oadrPrefix + "/health", exchange -> {
                System.out.println("health check handler called");
                exchange.sendResponseHeaders(200, -1);
                exchange.close();
            });
            server.createContext(oadrPrefix + "/EiRegisterParty", adrConn.eiRegisterParty);
            server.setExecutor(null);
            server.start();
            System.out.println("Server is listening on port " + port );
    }
    
    private void loadCertificate(){
        try {
            FileInputStream fIn = new FileInputStream(KEYSTORE_PATH);
            keystore = KeyStore.getInstance("PKCS12");
            keystore.load(fIn, KeyStorePass);
            // setup the key manager factory
            kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(keystore, KeyStorePass);
            FileInputStream fIn_trust = new FileInputStream(TRUSTSTORE_PATH);
            truststore = KeyStore.getInstance("PKCS12");
            truststore.load(fIn_trust, TrustStorePass);
            // setup the trust manager factory
            tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(truststore);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }
    }

    public void Stop() {
		server.stop(0);
		System.out.println("server stopped");
	}

}
