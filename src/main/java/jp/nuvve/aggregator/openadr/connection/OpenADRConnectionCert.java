package main.java.jp.nuvve.aggregator.openadr.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import main.java.jp.nuvve.aggregator.openadr.client.OpenADRHttpsClient;
import main.java.jp.nuvve.aggregator.openadr.log.HourlyRecordFileManager;
import main.java.jp.nuvve.aggregator.openadr.server.OpenADRHttpsServer;

/**
 * @author Nana Hayashi
 *
 */
public class OpenADRConnectionCert {
	private OpenADRHttpsClient client;
	private OpenADRHttpsServer server;
    public EiRegisterPartyHandler eiRegisterParty;

    private static Logger logger = Logger.getLogger(OpenADRConnectionCert.class.getName());
    private static HourlyRecordFileManager logManager;
    
    public OpenADRConnectionCert(){
//    	try {
//    		// setup communication log filestream
//    		FileHandler fHandler = new FileHandler("/Users/hayashinana/logs/communication.log", true);
//    		fHandler.setFormatter(new SimpleFormatter());
//    		logger.addHandler(fHandler);
//    		logger.setLevel(Level.FINE);
//
//    		// setup csv file manager
//    		String headings = "time,messagetype,src,dst,paramname,value";
//    		logManager = new HourlyRecordFileManager("oadr", "log", headings);
//    	} catch (IOException e) {
//    		logger.log(Level.WARNING, "couldn't open log file.", e);
//    	}

    	client = new OpenADRHttpsClient(false, false);
    	logger.info("OpenADRHttpsClient was created");
    	server = new OpenADRHttpsServer(this);
    	logger.info("OpenADRHttpsServer was created");
        logger.info("OadrEventManager was created");
        eiRegisterParty = new OpenADRConnectionCert.EiRegisterPartyHandler(this);
        logger.info("Oadr Event classes were created");
    }

    /**
     * setup and server run.
     */
    public void setup(){
        try {
			logger.info("setting up EventManager and server...");
			server.run();
			logger.info("Http server started succesfully");
	        logger.info("Http server started successfully");
		} catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
			logger.log(Level.WARNING, "error: setup server", e);
		}
    }
    
    /**
     * server stop
     */
    public void stop() {
        this.server.Stop();
    }
    
    /**
     * oadrQueryRegistration send.
     * @author Nana Hayashi
     */
    public synchronized void sendQueryRegistration() {
    	try {
    		logger.info("sending oadrQueryRegistration...");
		    String message = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
		    		+ "<ns3:oadrPayload\n"
		    		+ "  xmlns:ns1=\"http://docs.oasis-open.org/ns/energyinterop/201110\"\n"
		    		+ "  xmlns:ns10=\"http://docs.oasis-open.org/ns/emix/2011/06/siscale\"\n"
		    		+ "  xmlns:ns11=\"http://www.w3.org/2000/09/xmldsig#\"\n"
		    		+ "  xmlns:ns12=\"http://naesb.org/espi\"\n"
		    		+ "  xmlns:ns13=\"http://www.w3.org/2009/xmldsig11#\"\n"
		    		+ "  xmlns:ns14=\"urn:un:unece:uncefact:codelist:standard:5:ISO42173A:2010-04-07\"\n"
		    		+ "  xmlns:ns2=\"http://docs.oasis-open.org/ns/energyinterop/201110/payloads\"\n"
		    		+ "  xmlns:ns3=\"http://openadr.org/oadr-2.0b/2012/07\"\n"
		    		+ "  xmlns:ns4=\"urn:ietf:params:xml:ns:icalendar-2.0\"\n"
		    		+ "  xmlns:ns5=\"http://docs.oasis-open.org/ns/emix/2011/06\"\n"
		    		+ "  xmlns:ns6=\"urn:ietf:params:xml:ns:icalendar-2.0:stream\"\n"
		    		+ "  xmlns:ns7=\"http://www.w3.org/2005/Atom\"\n"
		    		+ "  xmlns:ns8=\"http://docs.oasis-open.org/ns/emix/2011/06/power\" xmlns:ns9=\"http://www.opengis.net/gml/3.2\">\n"
		    		+ "  <ns3:oadrSignedObject>\n"
		    		+ "    <ns3:oadrQueryRegistration ns1:schemaVersion=\"2.0b\">\n"
		    		+ "      <ns2:requestID>QueryRegistrationReqID120920_171330_001</ns2:requestID>\n"
		    		+ "    </ns3:oadrQueryRegistration>\n"
		    		+ "  </ns3:oadrSignedObject>\n"
		    		+ "</ns3:oadrPayload>";
		    logger.info("oadrQueryRegistration was sent");
    		logger.info("OadrLogs,oadrQueryRegistration,VEN,VTN,TEST,0");
    		logger.fine("message sent: " + message);
    		String response = this.client.sendPost(message, "EiRegisterParty");
    		//this.manager.unmarshallCreatedPartyRegistration(response);
    		logger.info("oadrCreatedPartyRegistration was received");
    		logger.info("OadrLogs,oadrCreatedPartyRegistration,VTN,VEN,TEST,0");
    		logger.fine("response message: " + response);
    	} catch (IOException e) {
    		logger.log(Level.WARNING, "error: oadrQueryRegistration", e);
    	} catch (RuntimeException e) {
    		logger.log(Level.WARNING, "error: oadrQueryRegistration", e);
    	}
    }
    
    /**
     * oadrCancelPartyRegistration receive.
     * @author Nana Hayashi
     *
     */
    public static class EiRegisterPartyHandler implements HttpHandler {
    	OpenADRConnectionCert adrConn;

    	public EiRegisterPartyHandler(OpenADRConnectionCert adrConn) {
    		this.adrConn = adrConn;
    	}

    	@Override
    	public void handle(HttpExchange he) throws IOException {
    		logger.info("EiRegisterPartyHandler reached");
    		String response = "";
    		String reportIdentifier = "";
    		String reportType = "";

    		try {
    			InputStreamReader isr = new InputStreamReader(he.getRequestBody(), "utf-8");
    			BufferedReader br = new BufferedReader(isr);
    			int b;
    			StringBuilder buf = new StringBuilder(512);
    			while ((b = br.read()) != -1) {
    				buf.append((char) b);
    			}
    			br.close();
    			isr.close();

    			String payload = buf.toString();
    			response = "";

    			he.getResponseHeaders().set("Content-Type", "application/xml; charset=utf-8");
    			he.sendResponseHeaders(200, response.length());
    			OutputStream os = he.getResponseBody();
    			os.write(response.getBytes());
    			os.close();
    			he.close();
    			logger.info("OadrLogs" + reportIdentifier);
    			logger.info("message sent: " + response); 
    		} catch (IOException e) {
    			logger.log(Level.WARNING, "error: register party handler", e);
    			logger.info("register party handler error");
    			System.out.println(e);
    			he.getResponseHeaders().set("Content-Type", "application/xml; charset=utf-8");
    			he.sendResponseHeaders(200, response.length());
    			OutputStream os = he.getResponseBody();
    			os.write(response.getBytes());
    			os.close();
    			he.close();
    			logger.info("OadrLogs,http200,VEN,VTN,TEST,0");
				logger.info(getCurrentTime() + ",http200,VEN,VTN,TEST,0");
    			logger.log(Level.WARNING, "oadr error response was sent");
    		}
    	}
    }

    /**
     * return current time.
     * @return strDate current time
     */
    public static String getCurrentTime(){
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }
}
