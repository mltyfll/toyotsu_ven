package main.java.jp.nuvve.aggregator.openadr.log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.TimeZone;


/**
 * Class that is responsible for managing log files that are rolled over every hour
 * on the hour. 
 * 
 * The log files will be stored in a given base (parent) directory under the
 * following directory structure:
 * 
 * <Base Directory>/logs/<type>/<year>/<month>/<date> 
 * 
 * and the filename will be:
 * 
 * log_for_hour_<hour>.csv
 * 
 * </pre>
 * <p>
 *   <b>Copyright 2013-2014, University of Delaware.  All rights reserved.</b>
 * </p>
 * @author Sachin Kamboj
 */
public class HourlyRecordFileManager {
	/** The type or name assigned to this log file */
	private String type;
	
	/** 
	 * The base directory in which the directory structure will be 
	 * created to store the log file.
	 */
	private String baseDirectory;
	
	/**
	 * A set of column headings to be added to the start of every
	 * log file when it is created
	 */
	private String headings;
	
	/**
	 * The PrintWriter used for writing data to the given log file.
	 */
	private PrintWriter out;
	
	
	/**
	 * The last hour that was logged. 
	 */
	private int lastHour = -1;
	
	/**
	 * Constructor used to define the parameters of the log files
	 * @param type The type or name assigned to the log file
	 * @param baseDirectory The base directory in which the logs will be stored
	 * @param headings The headings to be added to the start of every file
	 */
	public HourlyRecordFileManager(String type, String baseDirectory, String headings) {
		this.type = type;
		this.baseDirectory = baseDirectory;
		this.headings = headings;
	}
	
	/**
	 * Creates a new log file for the given hour
	 * @param cal The Calendar object used get the year, month, date and hour of the day
	 * @return The PrintWriter used to write to the log file. 
	 */
	private PrintWriter createLogFile(Calendar cal) {
		PrintWriter out;
		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		String str_hour = "" + hour;
		
		// Make hour always show two digits (e.g. 3 is 03)
		if (hour < 10) {
			str_hour = "0" + hour;
		}
		
		// Make the directories for storing the files
		String dirName = String.format("%s/logs/%s/%d/%d/%d", baseDirectory, type, year, month, date);
		new File(dirName).mkdirs();

		String filename = String.format("%s/log_for_hour_%s.csv", dirName, str_hour);
		try {
			if (new File(filename).exists()) {
				out = new PrintWriter(new FileWriter(filename, true));
			} else {
				File newfile = new File(filename);
				out = new PrintWriter(new FileWriter(filename));
				// print the header
				out.println(headings);
			}
		} catch (IOException e) {
			e.printStackTrace();
			out = null;
		}
		return out;
	}
	
	/**
	 * Returns a PrintWriter object that can be used to write to the current 
	 * log file - will automatically create a new log file if the hour is up
	 * @return The PrintWriter object
	 */
	public PrintWriter getWriter() {
		Calendar cal = Calendar.getInstance();
		// make the calendar use a UTC timezone so that it's consistent with the utc timestamps
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if (out == null) {
			lastHour = hour;
			out = createLogFile(cal);
		}
		else if (hour != lastHour) {
			lastHour = hour;
			out.close();
			out = createLogFile(cal);
		}
		return out;
	}
}
