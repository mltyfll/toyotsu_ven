# OpenADR 2.0b interface#

This is OpenADR 2.0b interface.  

This project provides the building blocks for implementing a VEN client.  In particular,
it aims to provide protocol bindings. 

## Development, Build, Testing ##

Use Maven: http://maven.apache.org

Build by running `mvn clean` and `mvn package` from this directory.  Dependencies will be downloaded automatically.  



** Note **: This interface only supports test case N0_5010_ in terms of sending payloads. After starting the server, type `5010` to send payload. It also receives the EiRegisterParty payload but does not respond.

** Note **: Change the values of the following variables according to the environment.

variables

    private static String KEYSTORE_PATH
    private static String TRUSTSTORE_PATH
    private static char[] KeyStorePass
    private static char[] TrustStorePass

files

    src/main/java/jp/nuvve/aggregator/openadr/client/OpenADRHttpsClient.java
    src/main/java/jp/nuvve/aggregator/openadr/server/OpenADRHttpsServer.java
